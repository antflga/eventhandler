package lol.Events;

import lol.BaseEvent.Event;

//Created by Antflga on 4/2/2015 at 10:09 PM for EventMagic
@SuppressWarnings("unused")
/**
 * These extenders of Event are left blank in this instance, but upon invocation of any related event the contents of this class will be called as well
 * as the event being flagged by the "@EventHandler" annotation.
 */
public class DemoEvent extends Event{

}
