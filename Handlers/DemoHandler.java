package lol.Handlers;

import lol.BaseEvent.EventHandler;
import lol.BaseEvent.Handler;
import lol.Events.DemoEvent;

//Created by Antflga on 4/2/2015 at 10:09 PM for EventMagic
@SuppressWarnings("unused")
/**
 * Handles the demo-event, overrides the generic Handler interfaces method and flags the method with the annotation. Both the constructor and event will be fired
 * upon rising of the event.
 */
public class DemoHandler implements Handler<DemoEvent>{

    @EventHandler("Demo Event")
    public void handle(DemoEvent event) {

    }
}
