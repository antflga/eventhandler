package lol.Handlers;

import lol.BaseEvent.EventHandler;
import lol.BaseEvent.Handler;
import lol.Events.RandomizerEvent;
import lol.Main;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Created by Antflga on 4/6/2015 at 1:12 AM for EventMagic
@SuppressWarnings("unused")
/**
 * Class was used to assign Pokemon to players for lulz. Actually useful for learning, I guess.
 * More in-depth extension of DemoHandler.
 */
public class RandomizerHandler implements Handler<RandomizerEvent> {

    public String grouping;

    /**
     * People participating in the game are listed here.
     */
    public List participants = Arrays.asList("Anthony", "Ty", "Rodolfo", "Merrick", "Dawson", "Homero", "Nolan");

    /**
     * Available starter pokemon are listed here.
     */
    public List pokemon = Arrays.asList("Bulbasaur", "Charmander", "Squirtle", "Chikorita", "Cyndaquil", "Totodile", "Turtwig", "Treecko", "Torchic", "Mudkip", "Snivy", "Tepig", "Oshawott");

    /**
     * @param event
     * Pass generic 't' to handling class. T is instance of a chosen event meant to be handled. This could all probably be done generically somewhat easily, too.
     * Overrides generic handler, flags method and sets it's string to "Randomizer"
     * Loops through and generates a set of random numbers, which it then uses to pair trainer to pokemon.
     */
    @Override
    @EventHandler("Randomizer")
    public void handle(RandomizerEvent event) {
        int loopSize = participants.size();

        //System.out.println(Integer.toString(loopSize));

        for (int i = 0; loopSize > i; i++) {
            participants = new ArrayList<String>(participants);
            pokemon = new ArrayList<String>(pokemon);

            int participantsInt = 0 + (int) (Math.random() * participants.size());
            int pokemonInt = 0 + (int) (Math.random() * pokemon.size());

            //System.out.println(Integer.toString(participants.size()) + " " + Integer.toString(pokemon.size()) + " " + Integer.toString(participantsInt) + " " + Integer.toString(pokemonInt));
            new Main().fileLog(this.getClass(), participants.get(participantsInt) + ":" + pokemon.get(pokemonInt));
            //System.out.println(participants.get(participantsInt) + ":" + pokemon.get(pokemonInt) + "\n");

            participants.remove(participantsInt);
            pokemon.remove(pokemonInt);
        }
    }
}
