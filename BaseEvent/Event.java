package lol.BaseEvent;

import lol.Main;

import java.util.ArrayList;
import java.util.List;

//Created by Antflga on 4/2/2015 at 10:03 PM for EventMagic
@SuppressWarnings("unused")
/**
 * Class used as a template so-to-speak
 * Is cancellable, is also titled event.
 * Whoop-de-doo.
 */
public class Event extends Cancellable {

    //This doesn't really have a use yet
    //It might come in handy sometime.
    public static List<Class<? extends Event>> eventlist = new ArrayList<>();

    /**
     * @param log
     * Useful for logging information.
     */
    @Deprecated
    public void log(String log){
        System.out.println("Event\n" +this.getClass().getSimpleName() + ": " + log);
    }

    /**
     * Log to console.
     * TODO: File logging.
     */
    public Event(){
        eventlist.add(this.getClass());
        new Main().fileLog(this.getClass(), this.getClass().getSimpleName() + " registered");
        //log("registered.");
        //System.out.println(this.getClass().getSimpleName() + " Has been registered.");
    }
}
