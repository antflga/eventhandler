package lol.BaseEvent;

//Created by Antflga on 4/2/2015 at 10:04 PM for EventMagic
@SuppressWarnings("unused")

/**
 * Cancellable only has two functions. To check and set the state of the boolean 'isCancelled'. The event publisher class checks if event
 * is cancelled and returns before invokation of the targeted method. Useful for some stuff, for instance ticks.
 */
public abstract class Cancellable {

    private boolean isCancelled = false;

    /**
     * @return
     * Returns state of boolean 'isCancelled'
     * False halts execution, true leaves it alone.
     */
    public boolean isCancelled() {
        return isCancelled;
    }

    /**
     * @param isCancelled
     * Sets the state of boolean 'isCancelled'.
     * See above method.
     */
    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }
}
