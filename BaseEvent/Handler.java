package lol.BaseEvent;

import java.util.*;

//Created by Antflga on 4/2/2015 at 10:06 PM for EventMagic
@SuppressWarnings("unused")
/**
 * It's used to construct handling-oriented classes.
 */

public interface Handler<T extends Event> {

        /**
         * @param event
         * Pass generic 't' to handling class. T is instance of a chosen event meant to be handled. This could all probably be done generically somewhat easily, too.
         * TODO:Maybe in the future auto-generate and auto-modify code for handler/event and base classes based on some input?
         */
        void handle(T event);
}
