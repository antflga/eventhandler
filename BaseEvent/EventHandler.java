package lol.BaseEvent;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//Created by Antflga on 4/2/2015 at 10:06 PM for EventMagic
@SuppressWarnings("unused")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
/**
 * Annotation class, pass it a string. Useful for searching for events in a class, or just organization stuffs.
 */
public @interface EventHandler {

    String[] value() default "Unspecified";
}
