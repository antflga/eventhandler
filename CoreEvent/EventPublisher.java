package lol.CoreEvent;

import lol.BaseEvent.Event;
import lol.BaseEvent.EventHandler;
import lol.Main;

import java.lang.reflect.Method;

//Created by Antflga on 4/2/2015 at 10:32 PM for EventMagic
@SuppressWarnings("unused")
/**
 * This is where events are "raised" so to speak. It just searches for the annotation in registered classes and uses reflection to invoke the method if it fits
 * certain parameters.
 */
public class EventPublisher {

    /**
     * @param log
     * Useful for logging information.
     */
    @Deprecated
    public static void log(String name, String log){
        System.out.println("Event Publisher\n" + name + ": " + log);
    }

    /**
     * @param event Extension of 'raise', public static allows for universal access and calls the passed event
     */
    public static void raiseEvent(final Event event) {
        new Main().fileLog(EventPublisher.class, "Raising event: " + event.getClass().getSimpleName());
        raise(event);
    }

    /**
     * @param event 'raise' iterates through a list of registered classes and searches for annotated methods to invoke.
     * TODO:This is bad and inefficient, audrey doesn't like it. Figure out what's bad and fix it.
     */
    private static void raise(final Event event) {
        for (Class handler : HandlerRegistry.getHandlers()) {

            Method[] methods = handler.getMethods();

            for (int i = 0; i < methods.length; ++i) {
                EventHandler eventHandler = methods[i].getAnnotation(EventHandler.class);
                if (eventHandler != null) {
                    Class[] methodParams = methods[i].getParameterTypes();
                    String[] methodFlag = eventHandler.value();

                    if (methodParams.length < 1) {
                        continue;
                    }

                    if (!event.getClass().getSimpleName().equals(methodParams[0].getSimpleName())) {
                        continue;
                    }

                    if (event.isCancelled()) {
                        //System.out.println("Event " + event.getClass().getSimpleName() + " was cancelled!");
                        new Main().fileLog(EventPublisher.class, event.getClass().getSimpleName() + " was cancelled!");
                        return;
                    }

                    try {
                        String name = event.getClass().getSimpleName();
                        methods[i].invoke(handler.newInstance(), event);
                        new Main().fileLog(EventPublisher.class, "Method " + methods[i].getName() + " invoked with annotation data '" + methodFlag[0] + "'");
                        //System.out.println("Method " + methods[i].getName() + " in Class " + event.getClass().getSimpleName() + " invoked with flag " + methodFlag[0]);
                    } catch (Exception e) {
                        System.err.println(e);
                    }
                }
            }
        }
    }
}
