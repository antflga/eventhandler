package lol.CoreEvent;

import lol.BaseEvent.Handler;
import lol.Main;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//Created by Antflga on 4/2/2015 at 10:32 PM for EventMagic
@SuppressWarnings("unused")
/**
 * Keeps track of handlers.
 */
public class HandlerRegistry{

    /**
     * List of handler classes.
     * EDIT: List removed, replaced with Queue, all further references to 'Handlers' should be noted that it has now changed to Queue format.
     */
    //private static List<Class> handlers = new ArrayList<Class>();

    private static Queue<Class<? extends Handler>> handlers = new LinkedList<>();

    /**
     * @param log
     * Useful for logging information.
     */
    @Deprecated
    public static void log(Class clazz, String log){
        System.out.println("Handler Registry\n" + clazz.getSimpleName() + ": " + log);
    }

    /**
     * @param clazz
     * Pass a class 'clazz' in to be added to handler registry.
     */
    public static void register(Class clazz){
        new Main().fileLog(clazz, "adding '" + clazz.getSimpleName() +"' to handler registry");
        handlers.add(clazz);
        //log(clazz, "added '" + clazz.getSimpleName() +"' to handler registry.");
    }

    /**
     * @param clazz
     * Inverse register.
     */
    public static void unRegister(Class clazz){
        new Main().fileLog(clazz, "removing '" + clazz.getSimpleName() + "' from handler registry");
        handlers.remove(clazz);
        //log(clazz, "removed from handler registry.");
    }

    /**
     * @return
     * Returns the <s>list</s> queue, useful for external access.
     */
    public static Queue<Class<? extends Handler>> getHandlers(){
        return handlers;
    }
}
