package lol;

import lol.CoreEvent.EventPublisher;
import lol.CoreEvent.HandlerRegistry;
import lol.Events.DemoEvent;
import lol.Events.Event1;
import lol.Events.Event2;
import lol.Events.RandomizerEvent;
import lol.Handlers.DemoHandler;
import lol.Handlers.EventHandler1;
import lol.Handlers.EventHandler2;
import lol.Handlers.RandomizerHandler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

//Created by Antflga on 4/2/2015 at 10:06 PM for EventMagic
@SuppressWarnings("unused")
/**
 * TODO: Event Priority
 * TODO: Make code better, optimize what can  be
 * TODO: Remove need to register events, Audrey has already demonstrated how, I just need to implement it to the project
 * TODO: Make annotations for organization of classes within the project, or classes extending the project. Have a class check for this information being annotated
 * TODO: and log it to the console/a text file appropriately.
 * TODO: As of 4/22 Event Priority is the major thing on my mind. Once that is figured out I can move on to the next thing.
 * TODO: 4/28, still no priority. But I have file logging! yay!
 */
public class Main {

    File outputDirectory = new File("output");

    public void fileLog(Class clazz, String dataToPass) {
        File outputFile = new File("output\\" + clazz.getSimpleName() + ".txt");
        BufferedWriter writer = null;

        if (!outputDirectory.exists()) {
            System.out.println(this.getClass().getSimpleName() + ": Creating directory '" + outputDirectory.getAbsolutePath() + "'");
            outputDirectory.mkdir();
        } else if (outputDirectory.exists()) {
            if (!outputFile.exists()) {
                System.out.println(this.getClass().getSimpleName() + ": Creating file '" + outputFile.getAbsolutePath() + "'");
                try {
                    outputFile.createNewFile();
                } catch (IOException e) {
                    System.err.println(e);
                }
            } else if (outputFile.exists()) {
                try {
                    writer = new BufferedWriter(new FileWriter(outputFile, true));
                    String reformattedData = dataToPass.replace("\n", "(newline)");
                    System.out.println("Writing data {" + reformattedData + "} " + "to file " + outputFile.getAbsolutePath());
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
                    Date dt = new Date();
                    String S = sdf.format(dt); // formats to 09/23/2009 13:53:28.238
                    writer.write(S + ": " + dataToPass + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        writer.close();
                    } catch(IOException e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    public static void main(String[] args){
        long originalMillis = System.currentTimeMillis();
        HandlerRegistry.register(RandomizerHandler.class);
        HandlerRegistry.register(DemoHandler.class);
        HandlerRegistry.register(EventHandler1.class);
        HandlerRegistry.register(EventHandler2.class);

        /**
         * Yay for events!
         */

        EventPublisher.raiseEvent(new RandomizerEvent());
        EventPublisher.raiseEvent(new DemoEvent());
        EventPublisher.raiseEvent(new Event1());
        EventPublisher.raiseEvent(new Event2());
    }
}
